#!/bin/bash
x=0

echo "Are you upgrading from a DAS masternode? Y/N"

while [ $x != 1 ]
do	
read yn
if [ "$yn" = "y" ] || [ "$yn" = "Y" ];
then
echo "Stopping your DAS masternode(s)..."
sleep 5s
cd
cd das
./das-cli stop
cd

echo "Pulling wallet files from Momijiscrypto's Github for the compiled wallet..."
git clone https://gitlab.com/Momiji/Tribe-mn

mkdir tribe
cp -R Tribe-mn/dasv012/. tribe
cd tribe
sleep 3s

echo "Starting the new Tribe wallet daemon..."
chmod +755 tribed && chmod +755 tribe-cli && chmod +755 tribe-tx
sleep 3s

./tribed -daemon
echo "Daemon started for 15 seconds..."
sleep 15s
./tribe-cli stop

cd
echo "Copying das masternode settings..."
cp .das/das.conf .tribe
cd .tribe && mv das.conf tribe.conf && cd
rm -r .das

echo "Updating your masternode..."
echo "deleting old client folders and old repository..."
sleep 5s
cd
rm -r DAS-Source
rm -r das
rm -r DAS-Masternode-script

cd tribe
./tribed -daemon
echo "Running 3 block count tests... It's okay to get an error"
sleep 5s
./tribe-cli getblockcount
sleep 5s
./tribe-cli getblockcount
sleep 5s
./tribe-cli getblockcount

echo "Setting up a crontab job to clear the debug log to preven your node from running out of space..."
sleep 3s
{ crontab -l; echo "* * * * * echo "\" \"" > /root/.tribe/debug.log"; } | crontab -
x=$(($x + 1));
elif [ "$yn" = "n" ] || [ "$yn" = "N" ];
then

echo "Stopping your Tribe masternode..."
cd && cd tribe && ./tribe-cli stop
sleep 5s
cd && cd Tribe-mn
git reset --hard
echo "Pulling new wallet files from Momiji's Gitlab"
git pull https://gitlab.com/Momiji/Tribe-mn
cd

cp -R Tribe-mn/dasv012/. tribe
cd tribe
sleep 3s

echo "Starting the new Tribe wallet daemon."
chmod +755 tribed && chmod +755 tribe-cli && chmod +755 tribe-tx
sleep 3s

./tribed -daemon
echo "Daemon started for 15 seconds..."
sleep 15s

echo "Running 3 blockcount tests... It's okay to get an error."
sleep 5s
./tribe-cli getblockcount
sleep 5s
./tribe-cli getblockcount
sleep 5s
./tribe-cli getblockcount
sleep 3s

./tribe-cli stop
x=$(($x + 1));
else
echo "Please enter Y or N"
fi; done

cd && cd Tribe-mn
git reset --hard

sleep 3s
echo "Please verify your blockcount is caught up with ./tribe-cli getblockcount.
Once confirmed, run ./tribe-cli masternode status."
echo "You can start your masternode up now by typing cd tribe, ./tribed -daemon and ./tribe-cli masternode status"





