#!/bin/bash

TARGZ='https://github.com/TribeCrypto/tribe/releases/download/v1.0.0/tribe-v1.0.0.1-ubuntu.tar.gz'
MOMIJI_REPO='git clone https://gitlab.com/Momiji/Tribe-mn/'
COIN_PORT=9399
COIN_PATH='/usr/local/bin'
BOOTSTRAP='https://static.tribe-crypto.com/bootstrap/bootstrap.dat'
COIN_NAME='Tribe'

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
CYAN='\033[0;36m'

##ADD CRONTAB CHECKING IF POSSIBLE
##MAYBE ADD AN OPTION TO SET THE COIN TO COIN_PATH IN OPTIONS
##ADD MULTI NODE SETUP -- fcheck should be complete now.

function fcheck(){
overall=0
folder3=0
folder2=0
folder=0
check=2
check2=3
while [ $overall != 1 ];
do
	read -p "How many masternodes do you plan on setting up? Enter 1, 2 or 3: " amount
	if [ "$amount" = '3' ];then
	while [ $folder3 != 3 ]; do
		if [ -d ../.tribe ] && [ -d ../.tribe$check ] && [ -d ../.tribe$check2 ];
		then
			echo "The maximum amount of folders exist already."
			overall=$(($overall+1));
			break
		elif [ -d ../.tribe ] && [ -d ../.tribe$check ];
		then
			mkdir ../.tribe$check2
			echo "Created the .tribe$check2 folder"
			folder3=$(($folder3+3));
			overall=$(($overall+1));
			break
		elif [ -d ../.tribe ] && [ ! -d ../.tribe$check ];then
			mkdir ../.tribe$check
			echo "Created the .tribe$check folder"
			folder3=$(($folder3+1));
		elif  [ ! -d ../.tribe ];
		then	
			mkdir ../.tribe
			echo ".tribe is now available"
			folder3=$(($folder3+1));
		fi;done
	elif [ "$amount"  = '2' ];then
	while [ $folder2 != 2 ];
	do
		if [ -d ../.tribe ] && [ -d ../.tribe$check ] && [ -d ../.tribe$check2 ];
		then
			echo "The maximum amount of folders exist already."
			overall=$(($overall+1));
			break
		elif [ -d ../.tribe ] && [ -d ../.tribe$check ];
		then
			mkdir ../.tribe$check2
			echo "Created the .tribe$check2 folder"
			folder2=$(($folder2+2));
			overall=$(($overall+1));
			break
			#~ break
		elif [ -d ../.tribe ] && [ ! -d ../.tribe$check ];then
			mkdir ../.tribe$check
			echo "Created the .tribe$check folder"
			folder2=$(($folder2+1));
			overall=$(($overall+1));
		elif  [ ! -d ../.tribe ];
		then	
			mkdir ../.tribe
			echo ".tribe is now available"
			folder2=$(($folder2+1));
	fi;done
	elif [ "$amount" = '1' ];then
	while [ $folder != 1 ];do
		if [ -d ../.tribe ] && [ -d ../.tribe$check ] && [ -d ../.tribe$check2 ];
		then
			echo "The maximum amount of folders exist already."
			overall=$(($overall+1));
			break
		elif [ -d ../.tribe ] && [ -d ../.tribe$check ];
		then
			mkdir ../.tribe$check2
			echo "Created the .tribe$check2 folder"
			folder=$(($folder+1));
			overall=$(($overall+1));
			break
		elif [ -d ../.tribe ] && [ ! -d ../.tribe$check ];then
			mkdir ../.tribe$check
			echo "Created the .tribe$check folder"
			folder=$(($folder+1));
			overall=$(($overall+1));
		elif  [ ! -d ../.tribe ];
		then	
			mkdir ../.tribe
			echo ".tribe is now available"
			folder=$(($folder+1));
			overall=$(($overall+1));
	fi;done
fi;done
	
	
	
}

function multi_node(){
	
	$MOMIJI_REPO
	fcheck
	#	new function
	#	If (.tribe) folders exist, check if the config files are already in place as well.
	#	If all 3 have the files, break.
	#	else if (continue the setup). If they are, skip to next folder to check until one doesn't have one. 
	
	
	
}

function multi_node_setup(){
#cd && cd $COIN_PATH
if	[ ! -f /usr/local/bin/tribed ] && [ ! -f /usr/local/bin/tribe-cli ] && [ ! -f /usr/local/bin/tribe-tx ];
then
	echo "Copying Tribe files to $COIN_PATH"
	cp tribed $COIN_PATH && cp tribe-cli $COIN_PATH && cp tribe-tx $COIN_PATH
fi;

#~ tribe-folder=1
g=2
h=3
d=0
echo "Do you plan on setting up more than one masternode? (Y/N)"
	read yn
	if [ "$yn" = 'Y' ] || [ "$yn" = 'y' ];
	then
		echo "Great!"
		echo -e "How many ${CYAN}$COIN_NAME${NC} masternodes are you wanting to setup? (Enter 2, or 3)"
		
		while [ $d != 1 ];
		do
		read -p "Number of nodes: " number_of_nodes
		if [ "$number_of_nodes" = '2' ];
		then
		##add a while loop to check if folders are there until it finds one that isn't
			
			#~ if [ ! -d ../.tribe ];
			#~ then
				#~ cd && mkdir .tribe
			#~ elif [ -d ../.tribe ];
			#~ then
				#~ 
				#~ cd && mkdir .tribe$g
				#~ elif [ -d ../.tribe ] && [ -d ../.tribe$g ];
				#~ then
				#~ g=$(($g + 1));
				#~ cd && mkdir .tribe$g
			#~ fi;
				#echo "2"
				
				tribed -daemon
				echo "Running 3 block count tests... It's okay to get an error."
				tribe-cli getblockcount
				sleep 5s
				tribe-cli getblockcount
				sleep 5s
				tribe-cli getblockcount
				
				echo "Stopping wallet daemon for rpc credentials..."
				tribe-cli stop
				sleep 3s
				cd && cd .tribe$g
				
				echo "masternode=1" >> tribe.conf
				echo "daemon=1" >> tribe.conf
				echo "listen=0" >> tribe.conf
				echo "Please enter your rpc credentials... "
				read -p "$rpcuser=" rpcuser
				#~ echo "rpcuser="
				#~ read rpcuser
				echo "rpcuser=$rpcuser" >>tribe.conf
				read -p "rpcpassword=" rpcpassword
				#~ echo "rpcpassword="
				#~ read rpcpassword
				echo "rpcpassword=$rpcpassword" >>tribe.conf
				echo "Enter any port except the one you use to connect to the vps or 9399"
				echo "Example: my second masternode will be set to port 9499 instead of 9399"
				read -p "rpcport=" rpcport
				#~ echo "rpcport="
				#~ read rpcport
				echo "rpcport=$rpcport" >>tribe.conf
				echo "Enter your new vps ip address"
				read -p "bind=" bind
				#~ echo "bind="
				#~ read bind
				echo "bind=$bind:$rpcport" >> tribe.conf
				echo "Enter your first masternode's public ip address."
				read -p "masternodeaddr=" vpsip
				#~ echo "masternodeaddr="
				#~ read vpsip
				echo "masternodeaddr=$vpsip:$COIN_PORT" >> tribe.conf
				read -p "Enter your second masternode pirvate key/Gen key: " mngenkey
				echo "masternodeprivkey=$mngenkey" >> tribe.conf
				cd && cp -r -p .tribe$g .tribe$h  #.tribe  .tribe$g
				
				echo "Setting up the next masternode."
				echo "Please enter a new rpc user. "
				read -p "rpcuser=" rpcuser2
				#~ echo "rpcuser="
				#~ read rpcuser3
				sed -r -i 's/rpcuser=.*/rpcuser='$rpcuser2'/g' .tribe$h/tribe.conf
				echo "Please enter a new rpc password"
				read -p "rpcpassword=" rpcpassword2
				#~ echo "rpcpassword="
				#~ read rpcpassword3
				sed -r -i 's/rpcpassword=.*/rpcpassword='$rpcpassword2'/g' .tribe$h/tribe.conf
				echo "Please enter a new port for this node"
				read -p "rpcport=" rpcport2
				#~ echo "rpcport="
				#~ read rpcport3
				sed -r -i 's/rpcport=.*/rpcport='$rpcport2'/g' .tribe$h/tribe.conf
				echo "Please enter your new ipv4 address for this node."
				read -p "bind=" bind2
				#~ echo "bind="
				#~ read bind3
				echo "bind=$bind2:$rpcport2" >> .tribe$h/tribe.conf
				sed -r -i 's/masternodeaddr=.*/masternodeaddr='$vpsip':'$COIN_PORT'/g' .tribe2/tribe.conf
				echo "Please enter the new masternode private key/genkey."
				echo "masternodeprivkey="
				read mngenkey3
				sed -r -i 's/masternodeprivkey=.*/masternodeprivkey='$mngenkey3'/g' .tribe2/tribe.conf
				
			d=$(($d + 1));
		elif [ "$number_of_nodes" = '3' ];
		then
			echo "3"
			d=$(($d + 1));
		else
			echo "Please enter 2 or 3."
		fi;done
		x=$(($x + 1));
	elif [ "$yn" = 'N' ] || [ "$yn" = 'n' ];
	then
		echo -e "Setting up additional ${CYAN}$COIN_NAME${NC} masternode"
		x=$(($x + 1));
	else
		echo "Please enter Y or N"
	fi;
	
	
	
	
	
	
	
	
	
#~ cd && cd Tribe-mn
#~ git reset --hard
#~ wget -O ../.tribe/bootstrap.dat $BOOTSTRAP
#~ rm -r ../.tribe/blocks && rm -r ../.tribe/chainstate
#~ echo "Setting up a crontab job to clear the debug log to preven your node from running out of space..."
#~ sleep 3s
#~ cron_tribe
}

function update_bootstrap(){
##Stops node(s). Deletes old bootstrap.dat if available. Downloads new bootstrap.dat
if [ -d ../.tribe ] && [ -d ../.tribe2 ] && [ -d ../.tribe3 ];
then
	tribe-cli stop
	tribe-cli -datadir=/root/.tribe2 stop
	tribe-cli -datadir=/root/.tribe3 stop
elif [ -d ../.tribe ] && [ -d ../.tribe2 ];
then
	tribe-cli stop
	tribe-cli -datadir=/root/.tribe2 stop
elif [ -d ../.tribe ];
then
	tribe-cli stop
else
	echo -e "${RED}No tribe nodes running"${NC}
	fi;

if [ -f ../.tribe/bootstrap.dat ] && [ -f ../.tribe2/bootstrap.dat ] && [ -f ../.tribe3/bootstrap.dat];
then
	rm -r ../.tribe/bootstrap.dat && rm -r ../.tribe2/bootstrap.dat && rm -r ../.tribe3/bootstrap.dat
	#rm -r ../.tribe/blocks && rm -r ../.tribe/chainstate && rm -r ../.tribe2/blocks && rm -r ../.tribe2/chainstate && rm -r ../.tribe3/blocks && rm -r ../.tribe3/chainstate
elif [ -f ../.tribe/bootstrap.dat ] && [ -f ../.tribe2/bootstrap.dat ];
then
	rm -r ../.tribe/bootstrap.dat && rm -r ../.tribe2/bootstrap.dat
	#rm -r ../.tribe/blocks && rm -r ../.tribe/chainstate && rm -r ../.tribe2/blocks && rm -r ../.tribe2/chainstate
elif [ -f ../.tribe/bootstrap.dat ];
then
	rm -r ../.tribe/bootstrap.dat
	#rm -r ../.tribe/blocks && rm -r ../.tribe/chainstate
else
	echo -e "${RED}No bootstrap found${NC}"
fi;
if [ -d ../.tribe ] && [ -d ../.tribe2 ] && [ -d ../.tribe3 ];
then
	wget -O ../.tribe/bootstrap.dat --progress=bar $BOOTSTRAP 
	cp -R ../.tribe/bootstrap.dat ../.tribe2
	cp -R ../.tribe/bootstrap.dat ../.tribe3
elif [ -d ../.tribe ] && [ -d ../.tribe2 ];
then
	wget -O ../.tribe/bootstrap.dat --progress=bar $BOOTSTRAP 
	cp -R ../.tribe/bootstrap.dat ../.tribe2
elif [ -d ../.tribe ];
then
	wget -O ../.tribe/bootstrap.dat --progress=bar $BOOTSTRAP 
else
	echo -e "${RED}No .tribe folders found..."${NC}
	fi;
}

function cron_tribe(){
	##Checks for multiple masternode setups on the machine before creating a new crontab for each individual debug.log
	if [ -d ../.tribe ] && [ -d ../.tribe2 ] && [ -d ../.tribe3 ];
	then
		{ crontab -l; echo "* * * * * echo "\" \"" > /root/.tribe/debug.log"; } | crontab -
	c=$(($c + 1));
		{ crontab -l; echo "* * * * * echo "\" \"" > /root/.tribe2/debug.log"; } | crontab -
	c=$(($c + 1));
		{ crontab -l; echo "* * * * * echo "\" \"" > /root/.tribe3/debug.log"; } | crontab -
	c=$(($c + 1));
	elif [ -d ../.tribe ] && [ -d ../.tribe2 ];
	then
		{ crontab -l; echo "* * * * * echo "\" \"" > /root/.tribe/debug.log"; } | crontab -
	c=$(($c + 1));
		{ crontab -l; echo "* * * * * echo "\" \"" > /root/.tribe2/debug.log"; } | crontab -
	c=$(($c + 1));
	else
		{ crontab -l; echo "* * * * * echo "\" \"" > /root/.tribe/debug.log"; } | crontab -
	c=$(($c + 1));
	fi;
	
}

function required(){
##Downloads neccessary files needed for the wallet to compile or function
echo "Checking for updates..."
apt-get update -y && apt-get upgrade -y
echo "Getting the necessarry dependencies..."
sleep 2
echo "Checking for distro upgrades..."
apt-get dist-upgrade -y
echo "Installing the nano text editor..."
apt-get install nano htop git -y
echo "Installing dev tools..."
apt-get install build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils -y
apt-get install libboost-all-dev -y
echo "Installing bitcoin repository..."
apt-get install software-properties-common -y
add-apt-repository ppa:bitcoin/bitcoin -y
apt-get update -y
apt-get install libdb4.8-dev libdb4.8++-dev -y
}

#~ function cli(){
#~ echo "Downloading the cli wallet..."
#~ git clone https://www.gitlab.com/momiji/Tribe-mn
#~ }

function start_nodes(){
##Checks for multiple masternode directories before attempting to start them.
	if [ -d ../.tribe ] && [ -d ../.tribe2 ] && [ -d ../.tribe3 ];
	then
	#~ tribed –daemon
	#~ tribed -datadir=/root/.tribe2 –daemon
	#~ tribed -datadir=/root/.tribe3 –daemon
	echo "All 3"
	elif [ -d ../.tribe ] && [ -d ../.tribe2 ];
	then
	#~ tribed –daemon
	#~ tribed -datadir=/root/.tribe2 –daemon
	echo "both" 
	elif [ -d ../.tribe ];
	then
	#~ tribed -daemon
	echo "Just the one"
	else
	echo -e ${RED}"None found. Please setup at least one masternode"${NC}
	fi;
}

function update(){
##Asks the user if they're upgrading for a DAS masternode before updating to Tribe.
##Updates Tribe masternodes to latest files if there's an update.
c=0
echo "Are you upgrading from a DAS masternode? Y/N"
while [ $c != 1 ];
do
read yn
if [ "$yn" = "y" ] || [ "$yn" = "Y" ];
then
	echo "Stopping your DAS masternode(s)..."
	sleep 5s
	cd
	cd das
	./das-cli stop
	cd

	echo "Pulling wallet files from Momijiscrypto's Gitlab for the compiled wallet..."
	$MOMIJI_REPO
	
	mkdir tribe
	cp -R Tribe-mn/dasv012/. tribe
	cd tribe
	sleep 3s

	echo "Starting the new Tribe wallet daemon..."
	chmod +755 tribed && chmod +755 tribe-cli && chmod +755 tribe-tx
	sleep 3s
	cp tribed $COIN_PATH && cp tribe-cli $COIN_PATH && cp tribe-tx $COIN_PATH
	
	tribed -daemon
	sleep 15s
	tribe-cli stop
	
	cd
	echo "Copying das masternode settings..."
	cp .das/das.conf .tribe
	cd .tribe && mv das.conf tribe.conf && cd
	rm -r .das
	
	echo "Updating your masternode..."
	echo "deleting old client folders and old repository..."
	sleep 5s
	cd
	rm -r DAS-Source
	rm -r das
	rm -r DAS-Masternode-script

	cd tribe
	tribed -daemon
	echo "Running 3 block count tests... It's okay to get an error"
	sleep 5s
	tribe-cli getblockcount
	sleep 5s
	tribe-cli getblockcount
	sleep 5s
	tribe-cli getblockcount
	cd && cd Tribe-mn
	git reset --hard

	echo "Setting up a crontab job to clear the debug log to preven your node from running out of space..."
	sleep 3s
	cron_tribe
	
elif [ "$yn" = "n" ] || [ "$yn" = "N" ];
then
	cd && cd tribe
	cp tribed $COIN_PATH && cp tribe-cli $COIN_PATH && cp tribe-tx $COIN_PATH
	echo "Stopping your Tribe masternode..."
	cd && tribe-cli stop
	cd Tribe-mn
	git reset --hard
	echo "Pulling new wallet files from Momiji's Gitlab"
	$MOMIJI_REPO
	cd
	cp -R Tribe-mn/dasv012/. tribe
	cd tribe
	sleep 3s

	echo "Starting the new Tribe wallet daemon."
	chmod +755 tribed && chmod +755 tribe-cli && chmod +755 tribe-tx
	cp tribed $COIN_PATH && cp tribe-cli $COIN_PATH && cp tribe-tx $COIN_PATH
	sleep 3s
	
	tribed -daemon
	echo "Daemon started for 15 seconds..."
	sleep 15s

	echo "Running 3 blockcount tests... It's okay to get an error."
	sleep 5s
	tribe-cli getblockcount
	sleep 5s
	tribe-cli getblockcount
	sleep 5s
	tribe-cli getblockcount
	sleep 3s

	tribe-cli stop
	c=$(($c + 1));
else
	echo "Please enter Y or N"
fi;done
	
	
}

function edit(){
##Checks for multiple .tribe folders for their individual tribe.conf files. Max 3 per machine
e=0
while [ $e != 1 ];
do
if [ -f ../.tribe/tribe.conf ] && [ -f ../.tribe2/tribe.conf ] && [ -f ../.tribe3/tribe.conf ];
#~ if [ `ls ../.tribe/tribe.conf` ] && [ `ls ../.tribe3/tribe.conf`  ] && [ `ls ../.tribe3/tribe.conf`  ];
then
	echo -e "1:${RED} ./tribe/tribe.conf${NC}" && echo -e "2:${RED} ./tribe2/tribe.conf${NC}" &&  echo -e "3:${RED} ./tribe3/tribe.conf"${NC}
elif [ `ls ../.tribe/tribe.conf`  ] && [ `ls ../.tribe2/tribe.conf`  ];
then
	echo -e "1: ${RED}.tribe/tribe.conf${NC}" && echo "2: ${RED}.tribe2/tribe.conf"${NC}
elif [ `ls ../.tribe/tribe.conf` ];
then
	echo -e "1: ${RED}./tribe/tribe.conf"${NC}
else
	echo "No tribe.conf(s) available."
fi;
read -p "Please make a selection: " choose 

if [ -f ../.tribe/tribe.conf ] && [ -f ../.tribe2/tribe.conf ] && [ -f ../.tribe3/tribe.conf ] && [ "$choose" = '1' ];
then
	nano ../.tribe/tribe.conf
	e=$(($e + 1));
elif [ -f ../.tribe/tribe.conf ] && [ -f ../.tribe2/tribe.conf ] && [ -f ../.tribe3/tribe.conf ] && [ "$choose" = '2' ];
then
	nano ../.tribe2/tribe.conf
	e=$(($e + 1));
elif [ -f ../.tribe/tribe.conf ] && [ -f ../.tribe2/tribe.conf ] && [ -f ../.tribe3/tribe.conf ] && [ "$choose" = '3' ];
then
	nano ../.tribe3/tribe.conf
	e=$(($e + 1));
elif [ -f ../.tribe/tribe.conf ] && [ -f ../.tribe2/tribe.conf ] && [ "$choose" = '1' ];
then
	nano ../.tribe/tribe.conf
	e=$(($e + 1));
elif [ -f ../.tribe/tribe.conf ] && [ -f ../.tribe2/tribe.conf ] && [ "$choose" = '2' ];
then
	nano ../.tribe2/tribe.conf
	e=$(($e + 1));
elif [ -f ../.tribe/tribe.conf ] && [ "$choose" = '1' ];
then
	nano ../.tribe/tribe.conf
	e=$(($e + 1));
else
	echo "Please make a valid selection"
fi;done
}

function reindex(){
##Checks for multiple masternode setups before attempting to reindex them
	if [ -d ../.tribe ] && [ -d ../.tribe2 ] && [ -d ../.tribe3 ];
	then
	tribed –reindex
	tribed -datadir=/root/.tribe2 –reindex
	tribed -datadir=/root/.tribe3 –reindex
	#echo "All 3"
	elif [ -d ../.tribe ] && [ -d ../.tribe2 ];
	then
	tribed –reindex
	tribed -datadir=/root/.tribe2 –reindex
	#echo "both" 
	elif [ -d ../.tribe ];
	then
	tribed -reindex
	#echo "Just the one"
	else
	echo "None to reindex"
	fi;
}

function first_setup(){
##Runs through the neccessary steps for the very first Tribe masternode setup
	echo "Creating new Tribe folder..."
	cd
	mkdir tribe
	cp -R Tribe-mn/dasv012/. tribe
	cd tribe
	sleep 3s
	echo "Starting the Tribe wallet daemon..."
	chmod +755 tribed && chmod +755 tribe-cli && chmod +755 tribe-tx
	sleep 3s
	cp tribed $COIN_PATH && cp tribe-cli $COIN_PATH && cp tribe-tx $COIN_PATH
	cd
	
	echo "Running first instance..."
	sleep 3s
	tribed -daemon
	echo "Running 3 block count tests... It's okay to get an error"
	sleep 5s
	tribe-cli getblockcount
	sleep 5s
	tribe-cli getblockcount
	sleep 5s
	tribe-cli getblockcount
	echo "Stopping wallet daemon for rpc credentials..."
	tribe-cli stop
	sleep 3s
	#cd
	cd .tribe
	echo "Please enter your rpc credentials... "
	echo "masternode=1" >> tribe.conf
	echo "daemon=1" >> tribe.conf
	echo "listen=0" >> tribe.conf
	echo "rpcuser="
	read rpcuser
	echo "rpcuser=$rpcuser" >>tribe.conf
	echo "rpcpassword="
	read rpcpassword
	echo "rpcpassword=$rpcpassword" >>tribe.conf
	echo "rpcport=$COIN_PORT" >>tribe.conf
	echo "Enter your masternode's public ip address."
	echo "masternodeaddr="
	read vpsip
	echo "masternodeaddr=$vpsip:$COIN_PORT" >> tribe.conf
	echo "Enter your masternode pirvate key/Gen key: "
	read mngenkey
	echo "masternodeprivkey=$mngenkey" >> tribe.conf
	
	
	cd && cd Tribe-mn
	git reset --hard
	wget -O ../.tribe/bootstrap.dat $BOOTSTRAP
	rm -r ../.tribe/blocks && rm -r ../.tribe/chainstate
	echo "Setting up a crontab job to clear the debug log to preven your node from running out of space..."
	sleep 3s
	cron_tribe
}

function choices(){
##Starting prompt
echo -e ${RED}"THIS IS A TEST BUILD"${NC}
echo "-------------------------------------------------------------"
echo -e "Welcome to the ${CYAN}Tribe${NC} Masternode setup script."
echo "Please make a selection below"

echo -e "1. Setup your ${GREEN}first${NC} ever masternode"
echo "2. Setup another masternode or masternodes"
echo "3. Masternode options"
echo -e "4. Start masternode(s)"
echo "Q. Quit"
echo "-------------------------------------------------------------"
read -p "Selection: " choice
}

function quit(){
	##Pointless
b=0
while [ $b != 1 ]
	echo "Do you want to quit the program now? (Y/N)"
	read ifquit
do
	if [ "$ifquit" = 'Y' ] || [ "$ifquit" = 'y' ];
	then
		exit
	elif [ "$ifquit" = 'N' ] || [ "$ifquit" = 'n' ];
	then
		clear
		#choices
		x=$(($x + 1));
		#b=$(($b + 1));
		break
	else
		echo "Please enter Y or N"

fi;done
}


function options(){
##Individual or multiple masternode options
	echo "-------------------------------------------------------------"
	echo "Please select and option you would like to do"
	echo "1. Edit tribe.conf(s)"
	echo "2. Re-index masternode(s)"
	echo "3. Upgrade node(s)"
	echo "4. Update bootstrap"
	echo "5. Go back"
	echo "-------------------------------------------------------------"
	read -p "Selection: " selection
	if [ "$selection" = '1' ];
	then
		if [ -d ../.tribe ] || [ -d ../.tribe2 ] || [ -d ../.tribe3 ];
		then
		echo "You currently have these tribe.conf files to edit:"
			edit
		elif [ ! -d ../.tribe ];
		then
		echo "Please set up a masternode"
		break
		else
		echo "Nothing here?"
		ls
		fi;
	elif [ "$selection" = '2' ];
	then
	echo "Re-indexing masternode(s)"
	reindex
	elif [ "$selection" = '3' ];
	then
		update
	elif [ "$selection" = '4' ];
	then
		update_bootstrap
	elif [ "$selection" = '5' ];
	then
		b=$(($b + 1));
	else
		echo "Please make a valid selection"
	fi;
}

a=0
while [ $a != 1 ]
do
choices

x=0
while [ $x != 1 ]
do
#read choice
if [ "$choice" = '1' ];
then
		required
		$MOMIJI_REPO
		first_setup
		x=$(($x + 1));
elif [ "$choice" = '2' ];
then
	required
	multi_node
	x=$(($x + 1));
elif [ "$choice" = '3' ];
then
	b=0
	while [ $b != 1 ]
	do
	options
	done
	x=$(($x + 1));
	
elif [ "$choice" = '4' ];
then
	echo -e "Locating masternodes..."
	start_nodes
	x=$(($x + 1));
elif [ "$choice" = 'q' ] || [ "$choice" = 'Q' ];
then
	exit
else
	echo "Please enter a choice listed above."
	x=$(($x + 1));
fi;done

#a=$(($a + 1));
done
